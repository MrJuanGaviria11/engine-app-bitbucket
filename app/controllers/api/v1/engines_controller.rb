class Api::V1::EnginesController < ApplicationController
  def index
    engine = Engine.all.order(created_at: :desc)
    render json: engine
  end

  def create
    engine = Engine.new(engine_params)

    if engine.save
      render json: engine
    else
      render json: engine.errors, status: :unprocessable_entity
    end
  end

  def show
    if engine
      render json: engine
    else
      render json: engine.try(:errors), status: :unprocessable_entity
    end
  end

  def destroy
    engine&.destroy
    render json: { message: 'Motorcycle deleted!' }
  end

  private

  def engine_params
    params.permit(:name, :description, :cc, :brand, :image)
  end

  def engine
    @engine ||= Engine.find_by_id(params[:id])
  end
end
