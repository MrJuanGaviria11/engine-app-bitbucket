Rails.application.config.assets.precompile += %w(
  application.js
  cable.js
  homepage.js
  application.css
)
