Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get 'engines/index'
      post 'engines/create'
      get '/show/:id', to: 'engines#show'
      delete '/destroy/:id', to: 'engines#destroy'
    end
  end
  root 'homepage#index'
  get '/*path' => 'homepage#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
