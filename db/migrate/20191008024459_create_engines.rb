class CreateEngines < ActiveRecord::Migration[5.2]
  def change
    create_table :engines do |t|
      t.string :name, null:false
      t.string :description, null:false
      t.integer :cc, null:false
      t.string :brand, null:false
      t.string :image, default: "https://techcrunch.com/wp-content/uploads/2019/06/BMW-Motorrad-Vision-DC-Roadster-12.jpg?w=730&crop=1"

      t.timestamps
    end
  end
end
