require 'rails_helper'

RSpec.describe Api::V1::EnginesController, type: :controller do

  let(:engine) { FactoryBot.create(:engine) }

  describe '#index' do
    describe 'valid' do
      subject { get :index }

      it 'success' do
        expect(subject).to be_success
      end
    end
  end

  describe 'show' do
    it 'success' do
      get :show, params: {id: engine.id}
      expect(response).to be_success
    end

    it 'error' do
      get :show, params: {id: 99999}
      expect(response.status).to eq(422)
    end
  end

  describe "create" do
    context "with valid attributes" do
      it "create new engine" do
        post :create, params: {"name"=>"GsxR600", "description"=>"Fast", "cc"=>"600", "brand"=>"Susuki", "image"=>"http://suzuki.com.pe/motos/wp-content/uploads/2016/11/GSX_R600L7_YSF_D-azul-1.png"}
        expect(Engine.count).to eq(1)
      end
    end

    context "with invalid attributes" do
      it "create new engine" do
        post :create, params: {}
        expect(response.status).to eq(422)
      end
    end
  end

  describe "destroy" do
    context "delete engine" do
      it "delete engine" do
        get :destroy, params: {id: engine.id}
        expect(Engine.count).to eq(0)
      end
    end
  end
end
