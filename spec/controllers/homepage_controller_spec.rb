require 'rails_helper'

RSpec.describe HomepageController, type: :controller do

  describe '#index' do
    describe 'valid' do
      subject { get :index }

      it 'success' do
        expect(subject).to be_success
      end
    end
  end
end