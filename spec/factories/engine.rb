FactoryBot.define do
  factory :engine do
    name         { "Motorcycle 1" }
    description  { "Fast" }
    cc           { 1000 }
    brand        {"Susuki"}
    image        {"https://techcrunch.com/wp-content/uploads/2019/06/BMW-Motorrad-Vision-DC-Roadster-12.jpg?w=730&crop=1"}
  end
end
