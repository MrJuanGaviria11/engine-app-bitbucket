require 'capybara/rspec'
require 'capybara-screenshot/rspec'
require 'selenium/webdriver'

if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start 'rails'
  puts "required simplecov"
end

RSpec.configure do |config|
  # rspec-expectations config goes here. You can use an alternate
  # assertion/expectation library such as wrong or the stdlib/minitest
  # assertions if you prefer.
  config.expect_with :rspec do |expectations|
    # This option will default to `true` in RSpec 4. It makes the `description`
    # and `failure_message` of custom matchers include text for helper methods
    # defined using `chain`, e.g.:
    #     be_bigger_than(2).and_smaller_than(4).description
    #     # => "be bigger than 2 and smaller than 4"
    # ...rather than:
    #     # => "be bigger than 2"
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended, and will default to
    # `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups

  Capybara.register_driver :selenium do |app|
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end

  Capybara.register_driver :headless_chrome do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: { args: %w(headless disable-gpu) }
    )

    Capybara::Selenium::Driver.new app, browser: :chrome, desired_capabilities: capabilities
  end

  Capybara.register_driver :selenium_chrome_headless_docker_friendly do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new
    browser_options.args << '--headless'
    browser_options.args << '--disable-gpu'
    browser_options.args << 'window-size=1024,768'
    # Sandbox cannot be used inside unprivileged Docker container
    browser_options.args << '--no-sandbox'
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
  end
  Capybara.javascript_driver = :selenium_chrome_headless_docker_friendly

  #Screenshot config
  Capybara.asset_host = 'http://localhost:3000'
  Capybara::Screenshot.webkit_options = { width: 1024, height: 768 }
  Capybara::Screenshot.register_driver(:exotic_browser_driver) do |driver, path|
    driver.browser.save_screenshot(path)
  end
end
